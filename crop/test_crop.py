import pytest
from PIL import Image

from crop.views import crop_center


@pytest.mark.parametrize('link', ['crop/test_images/big_city.jpg',
                                  'crop/test_images/small_car.jpg',
                                  'crop/test_images/car.png'])
def test_crop(link):
    img = Image.open(link)
    cropped = crop_center(img)
    print(cropped.size)
    assert cropped.size == (1000, 1000)
