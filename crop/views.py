import os

from PIL import Image
from django.shortcuts import render
from django.contrib.staticfiles.storage import staticfiles_storage

from crop.forms import ImageForm


def home(request):
    if request.method == 'POST':
        form = ImageForm(request.POST, request.FILES)
        if form.is_valid():
            img_data = form.cleaned_data.get("image")
            format = str(img_data).split('.')[1]
            img = Image.open(img_data)
            img.save(f'/PicCut/crop/static/crop/img/defolt.{format}')
            croped_img = crop_center(img)
            croped_img.save(f'/PicCut/crop/static/crop/img/crop.{format}')
            return render(request, 'crop/index.html', {'form': form, 'format': format})
    else:
        form = ImageForm()

    return render(request, 'crop/index.html', {'form': form})


def crop_center(pil_img) -> Image:
    crop_width = 1000
    crop_height = 1000

    img_width, img_height = pil_img.size
    return pil_img.crop(((img_width - crop_width) // 2,
                         (img_height - crop_height) // 2,
                         (img_width + crop_width) // 2,
                         (img_height + crop_height) // 2))
